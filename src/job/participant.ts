import {JobOptions} from '../job';
import {random, randomFloat, round} from '../utils/functions';
import {Lot, Transaction, TransactionType} from './interfaces';

export enum Participation {
  First = 'First',
  SkipFirst = 'SkipFirst',
  Alternate = 'Alternate',
  Chance = 'Chance',
  WantedChunks = 'WantedChunks',
}

export interface ParticipationOptions {
  wantedChunks?: number;
  chance?: number; // chance to participate
  chancedeviation?: number; // maximum divergence from participation chance for randomness
  method?: Participation[]; // determines with certainty whether or not participant will participate
  priceDeviation?: number; // maximum deviation from original price to bid for
}

export interface ParticipantOptions {
  id: number;
  balance?: number;
  chunks?: number;
  participation?: ParticipationOptions;
  chunkReward: number; // TODO make this dynamic (to simulate changes in pool compute power)
}

export interface ParticipantJson {
  id: number;
  balance: number;
  chunks: number;
  history: Transaction[];
  participation: ParticipationOptions;
}

export class Participant {
  id: number;
  balance: number;
  chunks: number;
  price = 0;
  tradeRound = 0;
  history: Transaction[];
  chunkReward: number;
  participation: ParticipationOptions;

  constructor(initialOptions: ParticipantOptions) {
    const {id, balance, chunks, participation, chunkReward} = {
      balance: random(1, 10),
      chunks: 0,
      participation: {},
      ...initialOptions,
    };

    this.id = id;
    this.balance = balance;
    this.chunks = chunks;
    this.participation = {
      method: [Participation.Chance],
      priceDeviation: 0.9,
      chance: randomFloat(0.5, 0.9),
      ...participation,
    };
    this.chunkReward = chunkReward;
    this.price = this.calculatePrice();
    this.history = [
      {
        amount: this.balance,
        balance: 0,
        timestamp: 0,
        description: TransactionType.None,
        ownedChunks: this.chunks,
      },
    ];
  }

  update(chunks: number, description: TransactionType, price: number) {
    const amount = (chunks || 1) * (price || this.price);

    this.balance += amount;
    this.chunks += chunks;
    if (this.participation.wantedChunks) {
      this.participation.wantedChunks -= chunks;
    }
    this.price = this.calculatePrice();
    this.history.push({
      amount,
      timestamp: this.tradeRound,
      balance: this.balance,
      description,
      ownedChunks: this.chunks,
    });
  }

  purchase(price: number) {
    this.update(1, TransactionType.Purchase, -price);
  }

  sale(amount?: number) {
    this.update(-1, TransactionType.Sale, -(amount || this.price));
  }

  reward(amount: number) {
    this.update(0, TransactionType.ChunkReward, amount);
  }

  tax(amount: number) {
    this.update(0, TransactionType.Tax, -amount);
  }

  calculatePrice() {
    const lower = this.chunkReward * 0.8;
    const upper = this.chunkReward * 1.02;
    return randomFloat(lower, upper);
  }

  bid(lot: Lot) {
    const participating = this.resolveParticipation(lot);
    if (!participating) {
      return NaN;
    }

    const minBid = 1.01 * lot.price;
    const maxBid = Math.min((1 + this.participation.priceDeviation!) * lot.price, this.balance);

    if (minBid >= maxBid) {
      // can't bid high enough
      return NaN;
    }

    const bid = randomFloat(minBid, maxBid);
    return bid;
  }

  private resolveParticipation({price, sellerId, highestBidder, originalPrice}: Lot): boolean {
    if (this.id === 0) {
      return false;
    }

    const {wantedChunks, chance, method, priceDeviation} = this.participation;

    if (
      sellerId === this.id || // dont buy your own chunk
      highestBidder === this.id || // dont compete against yourself
      price > this.balance || // must be able to afford
      price - originalPrice > priceDeviation! || // current price too high
      (wantedChunks && wantedChunks <= this.chunks) // if want for chunks fulfilled
    ) {
      return false;
    }

    const skipFirst = method?.includes(Participation.SkipFirst);
    const first = method?.includes(Participation.First);
    if (this.tradeRound === 0) {
      if (skipFirst) {
        return false;
      } else if (first) {
        return true;
      }
    }

    const methodResult = method?.reduce((participate, type) => {
      if (participate) {
        return participate;
      }

      switch (type) {
        case Participation.First:
          return this.tradeRound === 0;
        case Participation.SkipFirst:
          return this.tradeRound !== 0;
        case Participation.Alternate:
          const evenRound = this.tradeRound % 2 === 0;
          return (skipFirst && !evenRound) || evenRound; // if skipfirst, skip on odd rounds
        default:
          return true;
      }
    }, false);

    const rawChance = chance || 1;
    const roll = randomFloat(0, 1);

    return !!methodResult && roll < rawChance;
  }

  toJSON(): ParticipantJson {
    const {id, balance, chunks, history, participation} = this;
    return {id, balance, chunks, history, participation};
  }
}

export class PoolParticipant extends Participant {
  constructor(options: JobOptions) {
    const chunkReward = (options.block.reward * options.pool.computeShare) / options.pool.chunks;
    super({
      id: 0,
      chunkReward,
      balance: 0,
      chunks: options.pool.chunks,
      participation: {chance: 0},
    });
    this.balance = 0;
    this.price = chunkReward / 10;
  }

  tax(amount: number) {
    this.update(0, TransactionType.Tax, amount); // override tax to add instead of remove
  }

  reward(amount: number) {
    this.update(0, TransactionType.Reward, amount);
  }
}
