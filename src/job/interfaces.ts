export interface Transaction {
  balance: number;
  amount: number;
  description: TransactionType;
  ownedChunks: number;
  timestamp: number;
}

export const enum TransactionType {
  Purchase = 'Purchase',
  Sale = 'Sale',
  Reward = 'Reward',
  ChunkReward = 'ChunkReward',
  Tax = 'Tax',
  None = 'None',
}

export interface Bid {
  id: number; // id of bidder
  amount: number; // how much they're bidding
}

export interface Lot {
  price: number; // current price of item
  sellerId: number; // id of seller
  highestBidder?: number; // id of current highest bidder, eventually the buyer
  originalPrice: number; // original price at the beginning of the auction
}
