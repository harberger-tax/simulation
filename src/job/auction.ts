import {EventEmitter} from 'events';
import {Bid, Lot} from './interfaces';
import {Participant} from './participant';

export class Orchestrator {
  // TODO chose between open and closed auctions
  auction(lot: Lot, bidders: Bidder[]): {bidder: Bidder; bid: Bid} | null {
    // subscribe to valid bidders
    const bids = bidders
      .map((bidder) => ({bidder, bid: bidder.bid(lot)}))
      .filter(
        ({bidder, bid}) => bid.amount <= bidder.participant.balance && !isNaN(bid.amount), // some bidders wont bid
      );

    switch (bids.length) {
      case 0:
        console.log(`no buyer found`);
        return lot.highestBidder
          ? {bidder: bidders[lot.highestBidder], bid: {amount: lot.price, id: lot.highestBidder}}
          : null; // no buyer found

      case 1:
        console.log(`winning bid ${bids[0].bid.id} at ${bids[0].bid.amount}`);
        return bids[0]; // winning bid

      default: {
        // ignore ties since next round will give another chance
        const highest = bids.reduce((highest, current) =>
          current.bid.amount > highest.bid.amount ? current : highest,
        );
        return this.auction(
          {...lot, price: highest.bid.amount, highestBidder: highest.bid.id},
          bidders,
        );
      }
    }
  }
}

export class Bidder {
  participant: Participant;

  constructor(participant: Participant) {
    this.participant = participant;
  }

  bid(lot: Lot) {
    return {
      id: this.participant.id,
      amount: this.participant.bid(lot),
    };
  }
}
