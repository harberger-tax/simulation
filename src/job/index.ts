import {round} from '../utils/functions';
import {FileIO, IO} from '../utils/IO';
import {auction, calculateChunkReward, chunkPayout} from './actions';
import {Participant, ParticipantJson, ParticipationOptions, PoolParticipant} from './participant';

export interface Metrics {
  tradeRoundTime: number;
  tradeRoundCount: number;
  tradeCount: number;
  rewardCount: number;
  averageTradeTime: number;
}

export interface JobOptions {
  participants: number | ParticipationOptions[];
  block: {
    interval: number;
    reward: number;
  };
  trade: {
    interval: number;
  };
  pool: {
    chunks: number;
    tax: number;
    computeShare: number;
  };
  simulation: {
    rounds: number;
    scale: number;
    discountFactor?: number;
  };
}

export interface JobJson {
  options: JobOptions;
  metrics: Metrics;
  participants: ParticipantJson[];
}

export const defaultJobOptions: JobOptions = {
  participants: 20,
  block: {
    interval: 1,
    reward: 12.5,
  },
  trade: {
    interval: 0.5,
  },
  pool: {
    chunks: 50,
    tax: 0.02,
    computeShare: 0.3,
  },
  simulation: {
    rounds: 5,
    scale: 1000, // interval = 1 second
  },
};

export class Job {
  id: string;
  private io: IO;
  private options: JobOptions;
  private pool: PoolParticipant;
  metrics: Metrics;
  participants: Participant[];
  complete: boolean;
  analysed: boolean;

  constructor(id: string, io: IO, options: JobOptions) {
    this.io = io;
    this.id = id;
    this.options = options;
    this.complete = false;
    this.analysed = false;
    this.metrics = {
      tradeRoundTime: 0,
      tradeRoundCount: 0,
      tradeCount: 0,
      rewardCount: 0,
      averageTradeTime: 0,
    };
    const chunkReward = round(
      (options.block.reward * options.pool.computeShare) / options.pool.chunks,
    );
    this.pool = new PoolParticipant(options);
    this.participants = [this.pool];
    if (typeof options.participants !== 'number') {
      options.participants.forEach((participation, i) => {
        this.participants.push(new Participant({id: i + 1, chunkReward, participation}));
      });
    } else {
      while (this.participants.length < options.participants) {
        this.participants.push(
          new Participant({
            id: this.participants.length,
            chunkReward,
          }),
        );
      }
    }
  }

  async execute() {
    const blockInterval = Math.floor(this.options.block.interval / this.options.trade.interval);
    const blockIntervalFn = () => {
      this.metrics.rewardCount += 1;
      chunkPayout(this.participants, this.options, this.metrics.tradeRoundCount);
    };

    const tradeIntervalFn = () => {
      const start = Date.now();

      this.participants.forEach((participant) => {
        auction(participant, this.participants, this.metrics);
      });

      this.participants.forEach((participant) => {
        participant.tradeRound += 1;
      });

      const duration = Date.now() - start;
      this.metrics.tradeRoundCount += 1;
      this.metrics.tradeRoundTime += duration;
    };

    while (
      (this.options.simulation.discountFactor &&
        calculateChunkReward(this.options, this.metrics.tradeRoundCount) > 0.009) ||
      this.metrics.tradeRoundCount < this.options.simulation.rounds
    ) {
      console.log('round', this.metrics.tradeRoundCount);
      tradeIntervalFn();
      if (this.metrics.tradeRoundCount % blockInterval === 0) {
        blockIntervalFn();
      }
    }

    this.stop();
    return this;
  }

  setAnalysed() {
    this.analysed = true;
  }

  async stop() {
    this.complete = true;
    this.metrics.averageTradeTime = this.metrics.tradeRoundTime / this.metrics.tradeRoundCount;
    this.io.log({metrics: this.metrics, participants: this.participants});
    await this.io.close();
    console.log(this.metrics);
  }

  toJSON(): JobJson {
    const {options, metrics, participants} = this;
    return {
      options,
      metrics,
      participants: participants.map((participant) => participant.toJSON()),
    };
  }
}

if (require.main === module) {
  const job = new Job('job', new FileIO('log.json', true), defaultJobOptions);
  job.execute();
}
