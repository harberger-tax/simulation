import {randomFloat, round} from '../utils/functions';
import {Bidder, Orchestrator} from './auction';
import {JobOptions, Metrics} from './index';
import {Participant, PoolParticipant} from './participant';

// move chunks from one participant to another
export const transact = (
  buyer: Participant,
  owner: Participant,
  bidAmount?: number,
  chunks = 1,
) => {
  const amount = bidAmount || owner.price;
  console.log(
    `participant ${buyer.id} paid ${bidAmount ? bidAmount : `${chunks} x ${owner.price}`} to ${
      owner.id
    }`,
  );

  // add chunk to buyer
  buyer.purchase(amount);

  // remove chunk from seller
  owner.sale(amount);
};

export const taxCollection = (pool: Participant, taxee: Participant, tax: number) => {
  if (taxee.chunks < 1 || taxee.id === pool.id) return;

  const taxAmount = taxee.price * tax;
  // taxee loses blocks until they can afford the tax
  while (taxee.balance < taxAmount * taxee.chunks) {
    taxee.chunks -= 1;
    pool.chunks += 1;
    taxee.calculatePrice();
  }

  // pay the tax
  const amount = taxAmount * taxee.chunks;
  taxee.tax(amount);
  pool.tax(amount);
};

export const auction = async (
  seller: Participant,
  participants: Participant[],
  metrics?: Metrics,
) => {
  if (seller.chunks < 1) return;

  const lot = {
    sellerId: seller.id,
    originalPrice: seller.price,
    price: seller.price,
  };

  console.log(`--- auction for ${lot.sellerId} at ${lot.price} ---`);

  // random set of participants who can afford to purchase
  const bidders = participants.map((participant) => new Bidder(participant));

  const orchestrator = new Orchestrator();

  // find the winning bid
  const winning = orchestrator.auction(lot, bidders);

  // make the sale
  if (winning) {
    if (metrics) metrics.tradeCount += 1;
    transact(winning.bidder.participant, seller, winning.bid.amount);
  }

  console.log(`--------------------------------`);
};

export const calculateChunkReward = (options: JobOptions, roundCount: number) => {
  const chunkReward = (options.block.reward * options.pool.computeShare) / options.pool.chunks;
  const discount = options.simulation.discountFactor
    ? (1 / (1 + options.simulation.discountFactor)) ** roundCount
    : 1;
  return chunkReward * discount;
};
// simulated chunk payout
// TODO amortised reward
export const chunkPayout = (
  participants: Participant[],
  options: JobOptions,
  roundCount: number,
) => {
  const {computeShare, tax} = options.pool;

  const chunkReward = calculateChunkReward(options, roundCount);
  if (randomFloat(0, 1) >= computeShare) {
    return;
  }

  const [pool, ...receivers] = participants;

  const leftover = receivers.reduce((reward, receiver) => {
    if (receiver.id === 0) {
      return reward;
    }

    const amount = receiver.chunks * chunkReward;
    if (amount > 0) {
      receiver.reward(amount);
    }
    return reward - amount;
  }, chunkReward * options.pool.chunks);

  pool.reward(leftover);

  let totalTax = 0;
  participants.forEach((taxee) => {
    if (taxee.id == 0) {
      return;
    }

    const taxAmount = taxee.price * tax;
    if (taxee.chunks < 1 || taxee.id === pool.id) return;

    // taxee loses blocks until they can afford the tax
    while (taxee.balance < taxAmount * taxee.chunks) {
      taxee.chunks -= 1;
      pool.chunks += 1;
      taxee.calculatePrice();
    }

    // pay the tax
    const amount = taxAmount * taxee.chunks;
    taxee.tax(amount);
    totalTax += amount;
  });
  pool.tax(totalTax);
};

// actual payout of block
export const blockPayout = (
  reward: number,
  totalChunks: number,
  computeShare: number,
  participants: Participant[],
  metrics?: Metrics,
) => {
  // chance it was earned within the pool
  if (randomFloat(0, 1) < computeShare) {
    const pick = randomFloat(0, 1);
    let sum = 0;
    for (const participant of participants) {
      sum += participant.chunks / totalChunks;
      if (pick < sum) {
        if (metrics) metrics.rewardCount += 1;
        console.log(`****** rewarded ${participant.id} ${reward} ******`);
        participant.reward(reward);
        break;
      }
    }
  }
};
