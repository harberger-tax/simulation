import {defaultJobOptions} from '../job';
import {TransactionType} from '../job/interfaces';
import {Participant} from '../job/participant';

export interface Point {
  x: number;
  y: number;
}

export interface AnalysisDataSet {
  label?: string;
  data: Point[] | number[];
}

export interface AnalysisData {
  labels?: number[];
  datasets: AnalysisDataSet[];
}

export interface AnalysisResult {
  balances: AnalysisData;
  blockPurchases: AnalysisData;
  rewards: AnalysisData;
  chunks: AnalysisData;
}

export class Analysis {
  participants: Participant[];
  analysis: AnalysisResult;

  constructor(participants: Participant[]) {
    this.participants = participants;
    this.analysis = this.analyse(participants);
  }

  private analyse(participants: Participant[]) {
    const balances = [] as AnalysisDataSet[];
    const chunks = [] as AnalysisDataSet[];
    const blockPurchases = [] as number[];
    const rewards = [] as number[];

    const mapPoint = ([x, y]: [string, number]): Point => ({x: Number(x), y});

    const fill = (history: {[time: string]: number}) => {
      let last = 0;
      Object.entries(history).forEach(([x, y]) => {
        const current = Number(x);
        if (current - last < 1) return;
        for (let i = last + 1; i < current; i++) history[i] = y;
        last = current;
      });
    };

    participants.forEach((participant, i) => {
      const balanceHistory = {} as {[time: string]: number};
      const chunkHistory = {} as {[time: string]: number};
      participant.history.forEach((transaction) => {
        const {timestamp, balance, description, ownedChunks} = transaction;
        if (transaction.description === TransactionType.None) {
          return;
        }

        switch (description) {
          case TransactionType.None:
            break;
          case TransactionType.Purchase:
            blockPurchases.push(timestamp);
            break;
          case TransactionType.Reward:
            rewards.push(timestamp);
            break;
        }

        if (!chunkHistory[timestamp]) chunkHistory[timestamp] = 0;
        chunkHistory[timestamp] = ownedChunks;

        if (!balanceHistory[timestamp]) balanceHistory[timestamp] = 0;
        balanceHistory[timestamp] = balance;
      });

      fill(chunkHistory);
      chunks.push({
        data: Object.entries(chunkHistory).map(mapPoint),
        label: i > 0 ? `${participant.id}` : 'pool',
      });

      fill(balanceHistory);
      balances.push({
        data: Object.entries(balanceHistory).map(mapPoint),
        label: i > 0 ? `${participant.id}` : 'pool',
      });
    });

    const mapAvg = ([x, history]: [string, number[]]): Point => {
      const sum = history.reduce((total, value) => total + value);
      const avg = sum / history.length;
      return {x: Number(x), y: avg};
    };

    const mapDiff = ([x, history]: [string, number[]]): Point => {
      let max = 0;
      history.forEach((a, i) => {
        history.slice(i).forEach((b) => {
          const diff = Math.abs(a - b);
          if (diff > max) {
            max = diff;
          }
        });
      });
      return {x: Number(x), y: max};
    };

    const bin = (source: AnalysisDataSet[], width = 10) => {
      const result = [] as AnalysisDataSet[];
      source.forEach(({data, label}) => {
        const target = {} as {[time: string]: number[]};
        (data as Point[]).forEach(({x, y}) => {
          const binned = Math.floor(Number(x) / width) * width;
          if (!target[binned]) target[binned] = [];
          target[binned].push(y);
        });
        result.push({label, data: Object.entries(target).map(mapAvg)});
      });
      return result;
    };

    const mapAggregate = (
      source: AnalysisDataSet[],
      mapFn: ([x, history]: [string, number[]]) => Point,
    ) => {
      const target = {} as {[time: string]: number[]};
      source.slice(1).forEach(({data}) => {
        (data as Point[]).forEach(({x, y}) => {
          if (!target[x]) target[x] = [];
          target[x].push(y);
        });
      });

      return Object.entries(target).map(mapFn);
    };

    const binnedBalances = bin(balances, balances.length > 1000 ? 10 : 0);
    const binnedChunks = bin(chunks, chunks.length > 1000 ? 10 : 0);
    const balancesAvg = mapAggregate(binnedBalances, mapAvg);
    const chunksAvg = mapAggregate(binnedChunks, mapDiff);

    binnedBalances.push({
      data: balancesAvg,
      label: 'average',
    });

    binnedChunks.push({
      data: chunksAvg,
      label: 'maximum difference',
    });

    const binnedBlockPurchases = this.bin(blockPurchases);
    const binnedRewards = this.bin(rewards);

    return {
      balances: {
        datasets: binnedBalances,
      },
      chunks: {
        datasets: binnedChunks,
      },
      blockPurchases: {
        labels: binnedBlockPurchases.labels,
        datasets: [
          {
            label: 'block movement per trade interval',
            data: binnedBlockPurchases.data,
          },
        ],
      },
      rewards: {
        labels: binnedRewards.labels,
        datasets: [
          {
            label: 'rewards per block time',
            data: binnedRewards.data,
          },
        ],
      },
    } as AnalysisResult;
  }

  private bin(data: number[], width = 1) {
    const points = data.reduce((result, timestamp) => {
      const index = Math.floor(timestamp / width);
      while (result.length < index + 1) {
        result.push({x: result.length * width, y: 0});
      }
      result[index].y += 1;
      return result;
    }, [] as Point[]);
    return points.reduce(
      (result, point) => {
        result.labels.push(point.x);
        result.data.push(point.y);
        return result;
      },
      {labels: [] as number[], data: [] as number[]},
    );
  }

  result() {
    return this.analysis;
  }
}
