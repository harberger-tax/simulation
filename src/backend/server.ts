import bodyParser from 'body-parser';
import express from 'express';
import * as path from 'path';
import {defaultJobOptions, Job, JobJson, JobOptions} from '../job';
import {merge} from '../utils/functions';
import {DummyIO} from '../utils/IO';
import {Analysis, AnalysisResult} from './analysis';

export const configureServer = (jobQueue: {[key: string]: Job}) => {
  const analyse = (job: Job): AnalysisResult => new Analysis(job.participants).result();

  const app = express();
  const port = process.env.PORT || 4242;

  app.set('port', port);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(express.static(path.join(__dirname, 'static')));

  app.all('*', (req, _res, next) => {
    process.stdout.write(`${req.method} ${req.path} ${JSON.stringify(req.query)} ==> `);
    next();
  });

  app.get('/options', (_req, res) => {
    res.send(defaultJobOptions);
  });

  app.get('/update', (req, res) => {
    const id = req.query.id;
    if (!id || !jobQueue[id as string]) {
      console.log('job not found', id);
      res.sendStatus(404);
      return;
    }

    if (!jobQueue[id as string].complete) {
      res.send(204);
    }

    if (!jobQueue[id as string].analysed) {
      jobQueue[id as string].setAnalysed();
    }

    const result = analyse(jobQueue[id as string]);
    console.log('job analysed', id);
    res.send({result, job: jobQueue[id as string].toJSON()});
  });

  app.post('/new', (req, res) => {
    const id = req.query.id;
    console.log('body', req.body);
    if (id) {
      console.log(`new job request received: "${id}"`);
      const jobOptions: JobOptions = merge(defaultJobOptions, req.body || {});
      const job = new Job(id as string, new DummyIO(), jobOptions);
      jobQueue[id as string] = job;
      job.execute();
      res.send({id, jobOptions});
      return;
    }

    console.log(`job missing parameters: "${id}"`);
    res.sendStatus(404);
  });

  app.get('/json', (req, res) => {
    const result = {} as {[key: string]: JobJson};
    Object.keys(result).forEach((key) => {
      if (jobQueue[key].complete) {
        result[key] = jobQueue[key].toJSON();
      }
    });
    res.send(result);
  });

  return {
    app,
    start: (port: number) => new Promise((resolve) => app.listen(port, () => resolve(port))),
  };
};
