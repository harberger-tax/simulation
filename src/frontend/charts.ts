import Chart from 'chart.js';
import {AnalysisDataSet, AnalysisResult} from '../backend/analysis';
import {JobJson} from '../job';
import {Participation} from '../job/participant';
import {round} from '../utils/functions';

const root = document.querySelector('#root')!;

export const createMeta = (id: string, data: JobJson) => {
  const container = document.createElement('div');
  container.innerText += `job ${id} final results\n(${[
    `${data.metrics.tradeRoundCount} rounds`,
    `${data.metrics.tradeCount} trades`,
    `${data.metrics.rewardCount} rewards`,
  ].join(', ')})`;
  container.className = 'container meta-container';

  data.participants.forEach(({id, balance, chunks, participation}) => {
    const data = [`id: ${id}`, `balance: ${round(balance)}`, `chunks: ${chunks}`];
    if (participation.method) {
      const method =
        participation.method.join(', ') +
        (participation.method.includes(Participation.Chance) ? ` (${participation.chance})` : '');
      data.push(`method: ${method}`);
    }
    const div = document.createElement('div');
    div.id = `participant-${id}`;
    div.innerText = data.join(', ');
    container.appendChild(div);
  });
  const pre = document.createElement('pre');
  pre.innerText = JSON.stringify(data.options, null, 4);
  container.appendChild(pre);
  const history = document.createElement('div');
  history.className = 'history-container';
  const log = {} as {[timestamp: string]: string[]};
  data.participants.forEach(({id, history}) => {
    history.forEach((transaction) => {
      if (!log[transaction.timestamp]) {
        log[transaction.timestamp] = [];
      }
      const {amount, ownedChunks, balance, description} = transaction;
      log[transaction.timestamp].push(
        `${id}: ${ownedChunks}, ${round(balance)}, ${round(amount)}, ${description}`,
      );
    });
  });
  Object.keys(log).forEach((timestamp) => {
    const text = log[timestamp].map((el) => `\t(${el})`).join('\n');
    const div = document.createElement('div');
    div.className = 'log';
    div.innerText = `round: ${timestamp}\n${text}`;
    history.appendChild(div);
  });
  container.appendChild(history);
  root.appendChild(container);
};

const createCanvas = () => {
  const canvas = document.createElement('canvas');
  const container = document.createElement('div');
  container.className = 'container';
  container.appendChild(canvas);
  root.appendChild(container);
  return canvas;
};

const randomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

const createChart = (canvas: HTMLCanvasElement, config: Chart.ChartConfiguration) =>
  new Chart(canvas.getContext('2d')!, config);

const createDataSet = (data: AnalysisDataSet): Chart.ChartDataSets => {
  const color = randomColor();
  return {
    backgroundColor: color,
    fill: false,
    pointRadius: 0,
    borderColor: color,
    lineTension: 0,
    ...data,
  };
};

export const createCharts = (analysis: AnalysisResult) => {
  const {balances, chunks, blockPurchases, rewards} = analysis;

  const balancesChart = createChart(createCanvas(), {
    type: 'line',
    data: {
      datasets: balances.datasets.map((balance) => createDataSet(balance)),
    },
    options: {
      title: {
        text: 'Balance of participants',
        display: true,
      },
      maintainAspectRatio: false,
      responsive: true,
      scales: {
        xAxes: [
          {
            type: 'linear',
            position: 'bottom',
          },
        ],
      },
    },
  });

  const chunksChart = createChart(createCanvas(), {
    type: 'line',
    data: {
      datasets: chunks.datasets.map((chunk) => createDataSet(chunk)),
    },
    options: {
      title: {
        text: 'Chunks of participants',
        display: true,
      },
      maintainAspectRatio: false,
      responsive: true,
      scales: {
        xAxes: [
          {
            type: 'linear',
            position: 'bottom',
          },
        ],
      },
    },
  });

  const blockPurchasesChart = createChart(createCanvas(), {
    type: 'bar',
    data: {
      ...blockPurchases,
      datasets: blockPurchases.datasets.map((blockPurchases) => createDataSet(blockPurchases)),
    },
    options: {
      title: {
        text: 'Blocks purchased',
        display: true,
      },
      maintainAspectRatio: false,
      responsive: true,
    },
  });

  const rewardsChart = createChart(createCanvas(), {
    type: 'bar',
    data: {
      ...rewards,
      datasets: rewards.datasets.map((rewards) => createDataSet(rewards)),
    },
    options: {
      title: {
        text: 'Rewards received',
        display: true,
      },
      maintainAspectRatio: false,
      responsive: true,
    },
  });

  return {balancesChart, chunksChart, blockPurchasesChart, rewardsChart};
};
