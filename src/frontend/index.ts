import Chart from 'chart.js';
import {v4 as uuid} from 'uuid';
import {defaultJobOptions, JobOptions} from '../job';
import {createCharts, createMeta} from './charts';
import {newJob, updateJob} from './request';

interface RunnerOptions {
  interval?: number; // when to update
  duration?: number; // when to die?
}

type ChartMap = {[name: string]: Chart};

class Runner {
  private interval: number;
  jobs: {
    [id: string]: {
      active: boolean;
      charts?: ChartMap;
      updateInterval?: NodeJS.Timeout;
    };
  };

  constructor(options: RunnerOptions) {
    const {interval} = {
      interval: defaultJobOptions.simulation.scale,
      ...options,
    };

    this.jobs = {};
    this.interval = interval;
  }

  async initialize() {
    // await this.newJob(undefined, defaultJobOptions);
    return this;
  }

  async update(id?: string) {
    if (!id) {
      await Promise.all(
        Object.keys(this.jobs).map(async (id) => {
          if (this.jobs[id].active) {
            await this.update(id);
          }
        }),
      );

      return this;
    }

    console.log('update', id);
    const response = await updateJob({id});
    if (!response) {
      console.log(`no response for job ${id}`);
      return this;
    }

    console.log('job is complete', id, response);
    if (this.jobs[id].updateInterval) {
      clearInterval(this.jobs[id].updateInterval!);
      delete this.jobs[id].updateInterval;
      this.jobs[id].active = false;
    }

    if (!this.jobs[id].charts) {
      console.log('creating charts');
      createMeta(id, response.job);
      this.jobs[id].charts = createCharts(response.result);
    }

    return this;
  }

  async newJob(id?: string, options?: JobOptions) {
    const jobId = id || uuid();
    const response = await newJob(jobId, options);
    if (response) {
      console.log(`new job received: "${response.id}"`, response.jobOptions);
      this.jobs[jobId] = {
        active: true,
        updateInterval: setInterval(() => this.update(id), this.interval),
      };
      return response.id;
    }

    return null;
  }
}

(() => {
  const runner = new Runner({interval: 2000});
  (window as any).runner = runner;
  runner.initialize();
})();
