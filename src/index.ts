import 'source-map-support/register';
import {configureServer} from './backend/server';

const jobQueue = {};

const {start} = configureServer(jobQueue);

(async () => console.log('server listening on port', await start(4242)))();
