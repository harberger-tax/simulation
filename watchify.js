const fs = require('fs');
const watchify = require('watchify');
const browserify = require('browserify');
const tsify = require('tsify');

const b = browserify({
  cache: {},
  packageCache: {},
  plugin: [watchify, tsify],
  entries: 'src/frontend/index.ts',
}).on('log', console.log);

const bundle = (ids) => {
  if (ids) console.log(ids.join('\n'));
  b.bundle().on('error', console.error).pipe(fs.createWriteStream('dist/backend/static/index.js'));
};

b.on('update', bundle);
bundle();
